<?php

namespace Drupal\emergency_notification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Uuid\UuidInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class EmergencyNotificationConfigurationForm extends ConfigFormBase {

  /**
   * Drupal\Component\Uuid\UuidInterface definition.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * EmergencyNotificationConfigurationForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UuidInterface $uuid) {
    parent::__construct($config_factory);
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'emergency_notification_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'emergency_notification.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('emergency_notification.settings');

    $form['popup_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Emergency notification enabled'),
      '#default_value' => $config->get('popup_enabled'),
      '#description' => $this->t('Allows the emergency notification to be displayed of the site.'),
    ];

    $form['notice_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('notice_title'),
    ];

    $notice_text = $config->get('notice_text');
    $form['notice_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Notice text'),
      '#default_value' => $notice_text['value'],
      '#format' => $notice_text['format'],
    ];

    $form['open_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Open notice button text'),
      '#default_value' => $config->get('open_button_text'),
    ];

    $form['enable_colors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable colors'),
      '#default_value' => $config->get('enable_colors'),
      '#description' => $this->t('Enable the ability to set colors for custom styles.'),
    ];

    $form['overlay_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Overlay color'),
      '#default_value' => $config->get('overlay_color'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="enable_colors"]' => [['checked' => TRUE]],
        ],
      ],
    ];

    $form['background_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background color'),
      '#default_value' => $config->get('background_color'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="enable_colors"]' => [['checked' => TRUE]],
        ],
      ],
    ];

    $form['foreground_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Foreground color'),
      '#default_value' => $config->get('foreground_color'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="enable_colors"]' => [['checked' => TRUE]],
        ],
      ],
    ];

    $form['exclude_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude from paths'),
      '#default_value' => $config->get('exclude_paths'),
      '#description' => $this->t('Place a path pattern on new line. You can use "*" as a wildcard e.g. "/node/*".'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $this->config('emergency_notification.settings')
      ->set('popup_enabled', $values['popup_enabled'])
      ->set('notice_title', $values['notice_title'])
      ->set('notice_text', $values['notice_text'])
      ->set('exclude_paths', $values['exclude_paths'])
      ->set('open_button_text', $values['open_button_text'])
      ->set('enable_colors', $values['enable_colors'])
      ->set('overlay_color', $values['overlay_color'])
      ->set('background_color', $values['background_color'])
      ->set('foreground_color', $values['foreground_color'])
      ->set('form_submission_uuid', $this->uuid->generate())
      ->save();

    // Clear all caches.
    drupal_flush_all_caches();
  }

}
