# Emergency Notification

## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

## INTRODUCTION

Provides the ability to display a customisable emergency notification on all
pages, exempt the pages that are being excluded in configuration. Once the is
dismissed the emergency notification is still accessible through a button, that
is fixed to the bottom of the page.

* For a full description of the module, visit the project page:
https://www.drupal.org/project/emergency_notification

* To submit bug reports and feature suggestions, or track changes:
https://www.drupal.org/project/issues/emergency_notification

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

* Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


## CONFIGURATION

Navigate to Configuration > System > Emergency Notification settings.
* Configure settings, check "Emergency notification enabled" and save.
* Done!!! An emergency notification will now be visible on your site.


## MAINTAINERS

Current maintainers:
* [CbStuart](https://www.drupal.org/u/cbstuart)
